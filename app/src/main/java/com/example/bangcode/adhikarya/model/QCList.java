package com.example.bangcode.adhikarya.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bangcode on 7/17/2017.
 */

public class QCList {

    @SerializedName("result")
    List<QC> listQC= new ArrayList<>();

    public List<QC> getListQC() {
        return listQC;
    }

    public void setListQC(List<QC> listQC) {
        this.listQC = listQC;
    }

}
