package com.example.bangcode.adhikarya.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bangcode on 7/24/2017.
 */

public class QCImagesList {
    @SerializedName("result")
    List<QCimages> listQCimages= new ArrayList<>();

    public List<QCimages> getListQCimages() {
        return listQCimages;
    }

    public void setListQCimages(List<QCimages> listQCimages) {
        this.listQCimages = listQCimages;
    }
}
