package com.example.bangcode.adhikarya.model.authorization;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bangcode on 7/27/2017.
 */

public class User {
    @SerializedName("user_id")
    private int user_id;

    @SerializedName("token")
    private String token;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
