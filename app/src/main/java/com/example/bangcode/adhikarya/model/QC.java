package com.example.bangcode.adhikarya.model;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by bangcode on 7/12/2017.
 */

public class QC {


    @SerializedName("id")
    private Integer id;

    @SerializedName("name")
    private String name;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
