package com.example.bangcode.adhikarya.qcitem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.WindowManager;

import com.example.bangcode.adhikarya.R;
import com.example.bangcode.adhikarya.adapter.QCAdapter;
import com.example.bangcode.adhikarya.model.QC;
import com.example.bangcode.adhikarya.model.QCList;
import com.example.bangcode.adhikarya.service.ApiClient;
import com.example.bangcode.adhikarya.service.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FasilityActivity extends AppCompatActivity implements QCAdapter.ItemListener{

    private int id;
    private String name;
    ApiInterface mApiInterface;
    private RecyclerView mRecyclerView;
    private QCAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public static FasilityActivity fasilityActivity;
    private List<QC> mFasilityList;

    public static String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        getIntentExtras();

        this.setTitle("Fasilitas "+name);
        setContentView(R.layout.activity_fasility);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.fasility_recyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        fasilityActivity=this;
        refresh();
    }

    private void getIntentExtras(){
        Intent intent = getIntent();
        id = intent.getIntExtra("EXTRA_ID",0);
        name = intent.getStringExtra("EXTRA_NAME");
        token = intent.getStringExtra("EXTRA_TOKEN");
    }

    //get data dari retrofit
    public void refresh(){
        Call<QCList> fasilityCall= mApiInterface.getListItem(token,id);
        fasilityCall.enqueue(new Callback<QCList>() {
            @Override
            public void onResponse(Call<QCList> call, Response<QCList> response) {
                mFasilityList = response.body().getListQC();
                mAdapter = new QCAdapter(mFasilityList, getApplicationContext());
                mAdapter.setItemListener(FasilityActivity.this);
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<QCList> call, Throwable t) {

            }
        });
    }

    @Override
    public void onItemClick(int position) {
        QC qc= mFasilityList.get(position);
        Intent intent = new Intent(FasilityActivity.this, UnitActivity.class);

        intent.putExtra("EXTRA_ID", qc.getId());
        intent.putExtra("EXTRA_NAME", qc.getName());
        intent.putExtra("EXTRA_TOKEN", token);
        startActivity(intent);
    }

    @Override
    public void onLongItemClick(int position) {

    }
}
