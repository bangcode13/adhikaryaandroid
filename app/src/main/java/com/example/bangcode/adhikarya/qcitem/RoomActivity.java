package com.example.bangcode.adhikarya.qcitem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.WindowManager;


import com.example.bangcode.adhikarya.R;

import com.example.bangcode.adhikarya.adapter.QCAdapter;

import com.example.bangcode.adhikarya.model.QC;
import com.example.bangcode.adhikarya.model.QCList;
import com.example.bangcode.adhikarya.service.ApiClient;
import com.example.bangcode.adhikarya.service.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoomActivity extends AppCompatActivity implements QCAdapter.ItemListener{
    private int id;
    private String name;
    ApiInterface mApiInterface;
    private RecyclerView mRecyclerView;
    private QCAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public static RoomActivity rca;
    private List<QC> mRoomList;

    public static String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        getIntentExtras();

        this.setTitle("List Ruangan "+name);
        setContentView(R.layout.activity_room);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.room_recyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        rca=this;
        refresh();
    }

    private void getIntentExtras(){
        Intent intent = getIntent();
        id = intent.getIntExtra("EXTRA_ID",0);
        name = intent.getStringExtra("EXTRA_NAME");
        token = intent.getStringExtra("EXTRA_TOKEN");
    }

    //get data dari retrofit
    public void refresh(){
        Call<QCList> roomCall= mApiInterface.getListItem(token,id);
        roomCall.enqueue(new Callback<QCList>() {
            @Override
            public void onResponse(Call<QCList> call, Response<QCList> response) {
                mRoomList = response.body().getListQC();
                mAdapter = new QCAdapter(mRoomList, getApplicationContext());
                mAdapter.setItemListener(RoomActivity.this);
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<QCList> call, Throwable t) {

            }
        });
    }

    @Override
    public void onItemClick(int position) {
        QC qc= mRoomList.get(position);
        Intent intent = new Intent(RoomActivity.this, FasilityActivity.class);

        intent.putExtra("EXTRA_ID", qc.getId());
        intent.putExtra("EXTRA_NAME", qc.getName());
        intent.putExtra("EXTRA_TOKEN", token);
        startActivity(intent);
    }

    @Override
    public void onLongItemClick(int position) {

    }
}
