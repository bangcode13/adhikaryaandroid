package com.example.bangcode.adhikarya.service;


import com.example.bangcode.adhikarya.model.QCImagesList;
import com.example.bangcode.adhikarya.model.QCList;
import com.example.bangcode.adhikarya.model.authorization.Login;
import com.example.bangcode.adhikarya.model.authorization.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by bangcode on 7/17/2017.
 */

public interface ApiInterface {
    @GET("list-qc/0")
    Call<QCList> getQClist(@Header("Authorization") String authToken);

    @GET("list-qc/{id}")
    Call<QCList> getListItem(@Header("Authorization") String authToken,@Path("id") int id);

    @GET("list-document/{id}")
    Call<QCImagesList> getListImage(@Header("Authorization") String authToken,@Path("id") int id);

    //authorization
    @POST("auth/signin")
    Call<User> login(@Body Login login);
}

