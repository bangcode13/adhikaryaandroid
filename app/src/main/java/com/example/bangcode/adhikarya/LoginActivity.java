package com.example.bangcode.adhikarya;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bangcode.adhikarya.model.authorization.Login;
import com.example.bangcode.adhikarya.model.authorization.User;
import com.example.bangcode.adhikarya.service.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends Activity {
    public Button btnLogin;
    public User mUser;
    public String token;

    public static final String BASE_URL = "http://api.bangcode.net/api/";

    Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());

    Retrofit retrofit = builder.build();
    ApiInterface userClient = retrofit.create(ApiInterface.class);


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        init();
    }


//    public void init(){
//        btnLogin=(Button) findViewById(R.id.login);
//        btnLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent home= new Intent(LoginActivity.this, HomeActivity.class);
//                startActivity(home);
//            }
//        });
//    }

    public void init() {

        btnLogin = (Button) findViewById(R.id.login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                refresh();
            }
        });
    }

    public void refresh() {
        final EditText username = (EditText) findViewById(R.id.username);
        final EditText password = (EditText) findViewById(R.id.password);

        String uname = username.getText().toString();
        String pwd = password.getText().toString();

        Login login = new Login(uname, pwd);

        Call<User> userCall = userClient.login(login);
        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(LoginActivity.this, "login berhasil", Toast.LENGTH_SHORT).show();
                    token = "Bearer " + response.body().getToken();
                    Intent home = new Intent(LoginActivity.this, HomeActivity.class);
                    home.putExtra("EXTRA_TOKEN",token);
                    startActivity(home);
                } else {
                    Toast.makeText(LoginActivity.this, "Username atau password salah, login gagal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
