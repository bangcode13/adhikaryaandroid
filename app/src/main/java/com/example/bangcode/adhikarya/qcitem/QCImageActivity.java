package com.example.bangcode.adhikarya.qcitem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.WindowManager;

import com.example.bangcode.adhikarya.R;
import com.example.bangcode.adhikarya.adapter.qcitem.QCImageAdapter;
import com.example.bangcode.adhikarya.model.QCImagesList;
import com.example.bangcode.adhikarya.model.QCimages;
import com.example.bangcode.adhikarya.service.ApiClient;
import com.example.bangcode.adhikarya.service.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QCImageActivity extends AppCompatActivity {
    private int id;
    private String name;
    ApiInterface mApiInterface;
    private RecyclerView mRecyclerView;
    private QCImageAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public static QCImageActivity qcImageActivity;
    private List<QCimages> mImageList;

    public static String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qcimage);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        getIntentExtras();
        this.setTitle("List "+name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.qc_image_recyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        qcImageActivity=this;
        refresh();

    }

    //ambil data dari activity sebelumnya
    private void getIntentExtras(){
        Intent intent = getIntent();
        id = intent.getIntExtra("EXTRA_ID",0);
        name = intent.getStringExtra("EXTRA_NAME");
        token = intent.getStringExtra("EXTRA_TOKEN");
    }

    public void refresh(){
        Call<QCImagesList> imageCall= mApiInterface.getListImage(token,id);
        imageCall.enqueue(new Callback<QCImagesList>() {
            @Override
            public void onResponse(Call<QCImagesList> call, Response<QCImagesList> response) {
                mImageList = response.body().getListQCimages();
                mAdapter = new QCImageAdapter(mImageList, getApplicationContext());
//                mAdapter.setItemListener(JobActivity.this);
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<QCImagesList> call, Throwable t) {

            }
        });
    }
}
