package com.example.bangcode.adhikarya.adapter.qcitem;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bangcode.adhikarya.R;
import com.example.bangcode.adhikarya.model.QCimages;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by bangcode on 7/24/2017.
 */

public class QCImageAdapter extends RecyclerView.Adapter<QCImageAdapter.ViewHolder> {
    private List<QCimages> mQCListImage;
    private Context context;

    public QCImageAdapter(List<QCimages> mQCListImage, Context context) {
        this.mQCListImage = mQCListImage;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_image_qc, parent, false);
        ViewHolder mViewholder = new ViewHolder(mView);
        return mViewholder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        QCimages qcimage = mQCListImage.get(position);
        holder.mTextViewTitle.setText(mQCListImage.get(position).getTitle());
        Picasso.with(context)
                .load(context.getResources().getString(R.string.IMAGE_URL)+qcimage.getFilename())
                .into(holder.mImageViewQC);

    }

    @Override
    public int getItemCount() {
        return mQCListImage.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView mTextViewTitle;
        public ImageView mImageViewQC;

        public ViewHolder(View itemView) {
            super(itemView);
            mTextViewTitle = (TextView) itemView.findViewById(R.id.qc_item_title);
            mImageViewQC = (ImageView) itemView.findViewById(R.id.qc_item_image);
        }
    }
}
