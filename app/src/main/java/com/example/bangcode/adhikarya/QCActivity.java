package com.example.bangcode.adhikarya;


import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.bangcode.adhikarya.adapter.QCAdapter;
import com.example.bangcode.adhikarya.model.QC;
import com.example.bangcode.adhikarya.model.QCList;
import com.example.bangcode.adhikarya.qcitem.RoomActivity;
import com.example.bangcode.adhikarya.qcitem.ZoneActivity;
import com.example.bangcode.adhikarya.service.ApiClient;
import com.example.bangcode.adhikarya.service.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class QCActivity extends AppCompatActivity implements QCAdapter.ItemListener{

    ApiInterface mApiInterface;
    private RecyclerView mRecyclerView;
    private QCAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public static QCActivity qca;
    private List<QC> mQClist;

    public String token;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getIntentExtras();
        this.setTitle("List Lantai");
        setContentView(R.layout.activity_qc);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.qc_recyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        qca=this;
        refresh();

    }

    public void refresh(){
        Call<QCList> qcCall= mApiInterface.getQClist(token);
        qcCall.enqueue(new Callback<QCList>() {
            @Override
            public void onResponse(Call<QCList> call, Response<QCList> response) {
                mQClist = response.body().getListQC();
                mAdapter = new QCAdapter(mQClist, getApplicationContext());
                mAdapter.setItemListener(QCActivity.this);
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<QCList> call, Throwable t) {
                Toast.makeText(QCActivity.this,"gagal mengambil data",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getIntentExtras(){
        Intent intent = getIntent();
        token = intent.getStringExtra("EXTRA_TOKEN");

    }

    @Override
    public void onItemClick(int position) {
        QC qc= mQClist.get(position);
        Intent intent = new Intent(QCActivity.this, ZoneActivity.class);

        intent.putExtra("EXTRA_ID", qc.getId());
        intent.putExtra("EXTRA_TOKEN", token);
        intent.putExtra("EXTRA_NAME", qc.getName());
        startActivity(intent);
    }

    @Override
    public void onLongItemClick(int position) {

    }
}
