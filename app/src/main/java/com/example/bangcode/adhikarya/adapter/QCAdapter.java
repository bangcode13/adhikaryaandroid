package com.example.bangcode.adhikarya.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.example.bangcode.adhikarya.R;
import com.example.bangcode.adhikarya.model.QC;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by bangcode on 7/12/2017.
 */

public class QCAdapter  extends RecyclerView.Adapter<QCAdapter.ViewHolder>{
    private List<QC> mQCList;
    private Context context;
    private ItemListener itemListener;

    public QCAdapter(List<QC> mQCList, Context context) {
        this.mQCList = mQCList;
        this.context = context;

    }

    public void setItemListener(final ItemListener itemListener) {
        this.itemListener = itemListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_qc, parent, false);
        ViewHolder mViewholder = new ViewHolder(mView);
        return mViewholder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder,int position) {
        QC qc = mQCList.get(position);
        holder.mTextViewName.setText(mQCList.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return mQCList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        public TextView mTextViewId, mTextViewName;



        public ViewHolder(View itemView) {
            super(itemView);
            mTextViewName = (TextView) itemView.findViewById(R.id.tv_qc_name);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
           itemListener.onItemClick(getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            return true;
        }
    }

    private QC getItem(int id){
        return mQCList.get(id);
    }

    public interface ItemListener{
        void onItemClick(int position);
        void onLongItemClick(int position);
    }

}
